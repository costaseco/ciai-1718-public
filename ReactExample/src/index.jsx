"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
require("./index.css");
var Appt_1 = require("./Appt");
ReactDOM.render(<Appt_1.Hello compiler="TypeScript" framework="React"/>, document.getElementById("example"));
