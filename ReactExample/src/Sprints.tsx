import * as React from "react"
import { Task, TaskListWithDetailsAndForm } from './Tasks'
import {Component} from "react";

export interface Sprint { id:number, name: string; dueDate: string }

export const SprintList = (props:{ sprints: Sprint[], title:string }) =>
    <div>
        <h2>{props.title}</h2>
        <ul>
            { props.sprints.map(
                (sprint) => <li key={sprint.id}>{sprint.name} --- due: {sprint.dueDate}</li>
            )
            }
        </ul>
    </div>

export class NewSprintForm extends Component<{ addSprint:(sprint:Sprint)=>void },any> {

    count:number

    constructor(props: { addSprint: (sprint:Sprint)=>void }) {
        super()
        this.state = {
            name: "",
            dueDate:""
        }
        this.count = 0
        this.handleChangeName = this.handleChangeName.bind(this)
        this.handleChangeDate = this.handleChangeDate.bind(this)
    }

    addSprintHandler(e:any) {
        e.preventDefault()
        this.props.addSprint({id: this.count++, name: this.state.name, dueDate: this.state.dueDate})
    }

    handleChangeName(e:any) {
        const value = e.target.value
        this.setState( (state:any) => ({...state, name:value}))
    }

    handleChangeDate(e:any) {
        const value = e.target.value
        this.setState( (state:any) => ({...state, dueDate:value}))
    }

    render() {
        return (<div>
            <h2>New Sprint</h2>
            <form onSubmit={(e:any) => this.addSprintHandler(e)}>
                <label>
                    Sprint name:
                    <input type="text" name="name" onChange={this.handleChangeName} value={this.state.name}/>
                </label>
                <label>
                    Due date:
                    <input type="date" name="duedate" onChange={this.handleChangeDate} value={this.state.dueDate}/>
                </label>
                <input type="submit" value="Create"/>
            </form>
        </div>);
    }
}

export const ChooseSprintForm = () =>
    <div>
        <div>
            Selected task
        </div>
        <form>
            <input type="submit" value="Move to sprint"/>
            <select>
                <option>First</option>
                <option>Second</option>
            </select>
        </form>
    </div>

export const Sprints = (props: {sprints:Sprint[], tasks:Task[], addSprint:(sprint:Sprint)=>void, addTask:(task:Task)=>void } ) =>
    <div>
        <h1>Sprints</h1>
        <SprintList sprints={props.sprints} title={"Active Sprints"}/>
        <NewSprintForm addSprint={props.addSprint}/>
        <TaskListWithDetailsAndForm tasks={props.tasks} title={"Sprint tasks"} addTask={props.addTask}/>
    </div>

// TODO show title of the selected sprint
