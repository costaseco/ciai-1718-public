import * as React from "react";
import {Component} from "react";

export interface Task { id:number, name: string; dueDate:string }

export const TaskDetails = () =>
    <div>
        <h2>Task Details</h2>
        <div>
            <ul>
                <li>Name:</li>
                <li>Description:</li>
                <li>Due date:</li>
            </ul>
        </div>
    </div>

export const NewTaskForm = (props:{addTask:(task:Task)=>void}) =>
    <div>
        <h2>New Task</h2>
        <form>
            <label>
                Task name:
                <input type="text" name="name"/>
            </label>
            <label>
                Task description:
                <input type="text" name="description"/>
            </label>
            <label>
                Due date:
                <input type="date" name="duedate"/>
            </label>
            <input type="submit" value="Create"/>
        </form>
    </div>

export const TaskList = (props:{ tasks:Task[], title:string }) =>
    <div>
        <h2>{props.title}</h2>
        <ul>
            {props.tasks.map(
                (t: Task) => (<li key={t.id}>{t.name}</li>)
             )
            }
        </ul>
    </div>

const SearchCriteria = (props:{text:string, updateText:any}) =>
    <div className="inner-addon right-addon">
        <i className="glyphicon glyphicon-search"></i>
        <input type="text" name="search" onChange={props.updateText} className="form-control" placeholder="Search"/>
    </div>

export class SeacheableTaskList extends Component<{tasks:Task[], title:string},{text:string}> {
    constructor(props:{tasks:Task[]}) {
        super()
        this.state = {
            text:""
        }
    }

    updateText = (e:any) => this.setState({text:e.target.value})

    filterTasks = (tasks:Task[], text:string) => tasks.filter((t:Task) => t.name.indexOf(text) != -1);

    render() {
        return (<div>
                <h2>{this.props.title}</h2>
                <SearchCriteria text={this.state.text} updateText={this.updateText}/>
                <TaskList title={""} tasks={this.filterTasks(this.props.tasks, this.state.text)}/>
            </div>)
    }
}

export const TaskListWithDetailsAndForm = ( props:{tasks:Task[], title:string, addTask:(task:Task)=>void}) =>
    <div>
        <TaskList tasks={props.tasks} title={props.title}/>
        <TaskDetails/>
        <NewTaskForm addTask={props.addTask}/>
    </div>

