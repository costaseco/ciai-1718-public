import * as React from "react";
import { Task, SeacheableTaskList } from "./Tasks"
import {Link} from "react-router-dom";
import {Component} from "react";

export interface User { id:number, name:string }

export const UsersList = (props:{users:User[]}) =>
    <div>
        <h2>Users</h2>
        <ul>{
            props.users.map(
                (user) => <li key={user.name}><Link to={"/users/"+user.name}>{user.name}</Link></li>
            )
        }
        </ul>
    </div>

const UserStats = (props:{user:User}) =>
    props.user == null ?
        (<div></div>)
        :
        (<div><h2>Stats for {props.user.name}</h2></div>)

const UserDetails = (props:{user:User}) =>
    props.user == null ?
        (<div></div>)
        :
        (<div><h2>User</h2><b>Name:</b>{props.user.name}</div>)

export class UsersPage extends Component<{tasks:Task[], users:User[]}, {selectedUser:any}> {
    constructor(props:{tasks:Task[], users:User[]}) {
        super()
        this.state = {
            selectedUser : null
        }
        console.log((props as any).params)
    }

    selectUser(user:User) {
        this.setState({ selectedUser: user})
    }

    render() {
        return (
            <div>
                <UsersList users={this.props.users}/>
                <UserDetails user={this.state.selectedUser}/>
                <UserStats user={this.state.selectedUser}/>
                <SeacheableTaskList tasks={this.props.tasks} title={"My Tasks"}/>
            </div>
        )
    }
}
