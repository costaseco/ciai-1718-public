import * as React from "react";
import {Component} from "react";

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import { Sprint, Sprints, SprintList, ChooseSprintForm } from './Sprints'

import { Task, TaskList, SeacheableTaskList, NewTaskForm } from './Tasks'

import { User, UsersPage } from './Users'

const DashboardPage = ( props:{ sprints:Sprint[], tasks:Task[], users:User[]} ) =>
    <div>
        <h1>Dashboard</h1>
        <SprintList sprints={props.sprints} title={"Active Sprints"}/>
        <SeacheableTaskList tasks={props.tasks} title={"My Tasks"}/>
    </div>

const BackLogPage = (props:{ tasks:Task[], addTask:(task:Task)=>void}) =>
    <div>
        <TaskList tasks={props.tasks} title={"Backlog"}/>
        <NewTaskForm addTask={props.addTask}/>
        <ChooseSprintForm/>
    </div>

const Menu = () =>
    <div className="navbar navbar-default">
        <div className="container">
            <div className="navbar-header">
                <div className="navbar-brand">Task List</div>
                <button className="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                </button>
            </div>
            <div className="navbar-collapse collapse" id="navbar-main">
                <ul className="nav navbar-nav">
                    <li><Link to="/"> Dashboard </Link></li>
                    <li><Link to="/sprints"> Sprints </Link></li>
                    <li><Link to="/backlog"> BackLog </Link></li>
                    <li><Link to="/users"> Users </Link></li>
                </ul>
            </div>
        </div>
    </div>

const Layout = (props:any) => (
            <div>
                <Menu/>
                <div className={"container"}>
                    {props.children}
                </div>
            </div>)

export class App extends Component<{},{sprints:Sprint[], tasks:Task[], users:User[]}> {

    constructor(props: any) {
        super(props);
        this.state = {
            sprints: [
                {id: 1, name:"First Prototype", dueDate: "2017-10-30"},
                {id: 2, name:"Second Prototype", dueDate: "2017-11-30"},
                {id: 3, name:"Alpha version", dueDate: "2017-12-30"},
                {id: 4, name:"Going into production", dueDate: "2018-1-30"},
            ],
            tasks: [
                {id: 1, name:"Go shopping", dueDate:"2017-10-23"},
                {id: 2, name:"Do the dishes", dueDate:"2017-10-22"},
                {id: 3, name:"Do homework", dueDate:"2017-10-21"},
            ],
            users: [
                {id: 1, name: "John"},
                {id: 2, name: "Mary"},
                {id: 3, name: "Dave"},
            ]
        }
    }

    addSprint = (sprint:Sprint) =>
        (this.setState((state:any)=>({sprints:[sprint,...state.sprints], tasks:state.tasks, users:state.users})))

    addTask = (task:Task) =>
        (this.setState((state:any)=>({sprints:state.sprints, tasks:[task,...state.tasks], users:state.users})))

    render() {
        return (
            <Router>
                <Layout>
                    <Route key={"dashboard"} exact path="/" component={() => <DashboardPage sprints={this.state.sprints} tasks={this.state.tasks} users={this.state.users}/>}/>
                    <Route key={"sprints"} path="/sprints" component={() => <Sprints sprints={this.state.sprints} tasks={this.state.tasks} addSprint={this.addSprint} addTask={this.addTask}/>}/>
                    <Route key={"backlog"} path="/backlog" component={() => <BackLogPage tasks={this.state.tasks} addTask={this.addTask}/>}/>
                    <Route key={"users"} path="/users/:username?" component={() => <UsersPage tasks={this.state.tasks} users={this.state.users}/>}/>
                </Layout>
            </Router>);
    }
}

