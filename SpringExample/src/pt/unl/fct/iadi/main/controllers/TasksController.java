package pt.unl.fct.iadi.main.controllers;

import org.springframework.web.bind.annotation.*;
import pt.unl.fct.iadi.main.model.Task;

import java.util.HashMap;

// Inspired in: https://spring.io/guides/gs/rest-service/

@RestController
@RequestMapping(value="/tasks")
public class TasksController {

    // Using a simple storage for the purpose of illustrating a controller
    // The storage and manipulation of tasks will afterwards be factorized
    // in a Service class
    HashMap<Integer,Task> tasks = new HashMap<>();
    private int tasksSequence = 0;


    @RequestMapping(value="", method= RequestMethod.GET)
    Task[] getAll(@RequestParam(required=false, value="") String search) {
        return search == null || search.equals("") // just in case
                ?
                tasks.values().toArray(new Task[]{})
                :
                tasks
                .values()
                .stream()
                .filter(t -> t.getDescription().contains(search))
                .toArray(Task[]::new);
    }


    @RequestMapping(value="", method = RequestMethod.POST)
    void createTask(@RequestBody Task t) {
        t.setId(++tasksSequence);
        Task.valid(t);

        tasks.put(t.getId(), t);
    }


    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    Task showTask(@PathVariable int id) {
        Task t = tasks.get(id);
        Preconditions.checkFound(t);

        return t;
    }


    @RequestMapping(value="/{id}", method = RequestMethod.POST)
    void updateTask(@PathVariable int id, @RequestBody Task t) {
    		Preconditions.checkCondition(t.getId()==id);
    		Task t2 = tasks.get(id);
    		Preconditions.checkFound(t2); 
        Task.valid(t);
        
        tasks.put(id, t); // updates the existing task with the request's info
    }


    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    void deleteTask(@PathVariable int id) {
        Task t = tasks.get(id);
        Preconditions.checkFound(t);

        tasks.remove(id);
    }
}
