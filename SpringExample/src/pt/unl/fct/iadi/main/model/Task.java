package pt.unl.fct.iadi.main.model;

import pt.unl.fct.iadi.main.exceptions.BrokenPrecondition;

import java.util.Date;

public class Task {
    int id;
    String description;
    Date creationDate;
    Date dueDate;

    public Task() {}

    public Task(int id, String description, Date creationDate, Date dueDate) {
        this.id = id;
        this.description = description;
        this.creationDate = creationDate;
        this.dueDate = dueDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public static void valid(Task t) {
        if( t.getId() == 0 ||
            t.getDescription() == null ||
            t.getCreationDate() == null ) {
            // can also tests dueDate >= creationDate
            throw new BrokenPrecondition();
        }
    }
}
