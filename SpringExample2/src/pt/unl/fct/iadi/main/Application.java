package pt.unl.fct.iadi.main;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import pt.unl.fct.iadi.main.model.Task;
import pt.unl.fct.iadi.main.model.TaskRepository;

// Inspired in: https://spring.io/guides/gs/spring-boot/
// also in: https://spring.io/guides/gs/accessing-data-jpa/

@SpringBootApplication
public class Application {

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx, TaskRepository tasks) {
        return args -> {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            tasks.save(new Task(1,"First",sdf.parse("2017-10-20"), sdf.parse("2017-10-22")));
            tasks.save(new Task(2,"Second",sdf.parse("2017-10-21"), sdf.parse("2017-10-23")));
            tasks.save(new Task(3,"Third",sdf.parse("2017-10-22"), sdf.parse("2017-10-24")));
            tasks.save(new Task(4,"Forth",sdf.parse("2017-10-23"), sdf.parse("2017-10-25")));

            // fetch all tasks
            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            for (Task t : tasks.findAll()) {
                log.info(t.toString());
            }
            log.info("");

            // fetch one task
            log.info("Customers found with findOne(1):");
            log.info("-------------------------------");
            log.info(tasks.findOne(1).toString());
            log.info("");

            // fetch one task by description
            log.info("Customers found with findByDescription(1):");
            log.info("-------------------------------");
            for (Task t : tasks.findByDescription("First")) {
                log.info(t.toString());
            }
            log.info("");
        };
    }

}